<?php 

use Page\ShoesPage;
use Page\BagOverview;
use Page\Checkout;

class HomeworkCest
{
    public function purchaseItem(AcceptanceTester $I)
    {
        $I->wantTo('purchase an item.');

        $I->amOnPage(ShoesPage::$nikesForPurchaseURL);
        $I->click(ShoesPage::$popUpCloseButton);

        $I->waitForElement(ShoesPage::$productImage);
        $I->click(ShoesPage::$shoesSizeSelector);
        $I->click(ShoesPage::$addToBagButton);
        $I->click(ShoesPage::$bagButton);

        $I->click(BagOverview::$continueButton);
        $I->fillField(Checkout::$guestEmailInput, TesterData::$person['email']);
        $I->click(Checkout::$newUserContinueButton);

        $I->fillName(TesterData::$person);
        $I->click(Checkout::$enterAddressManuallyLink);
        $I->fillAddress(TesterData::$address);
        $I->click(Checkout::$toDeliveryButton);
        $I->click(Checkout::$standartDeliveryRadioButton);
        $I->click(Checkout::$toPaymentButton);
        $I->click(Checkout::$payWithCardButton);

        $I->switchToIFrame(Checkout::$cardIFrame);
        $I->fillCardData(TesterData::$visa);
        $I->click(Checkout::$toConfirmButton);
        $I->switchToIFrame();

        $I->waitForElement(Checkout::$toSummaryButton);
        $I->click(Checkout::$toSummaryButton);
        $I->seeInCurrentUrl('/checkout/payment?errorcode=101&ct=2');
    }
}
