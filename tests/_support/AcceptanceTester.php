<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
use Page\Checkout;

class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    public function fillName($person)
    {
        $this->fillField(Checkout::$firstNameInput, $person['firstName']);
        $this->fillField(Checkout::$lastNameInput, $person['surname']);
    }

    public function fillAddress($address)
    {
        $this->fillField(Checkout::$addressFirstLine, $address['firstLine']);
        $this->fillField(Checkout::$addressTown, $address['city']);
        $this->fillField(Checkout::$addressZip, $address['zip']);
        $this->fillField(Checkout::$addressMobile, $address['phone']);
    }

    public function fillCardData($card)
    {
        $this->fillField(Checkout::$cardNumber, $card["number"]);
        $this->fillField(Checkout::$cardExpiryMonth, $card["expMonth"]);
        $this->fillField(Checkout::$cardExpiryYear, $card["expYear"]);
        $this->fillField(Checkout::$cardCVV, $card["cvv"]);
    }
}
