<?php

namespace Page;

class Checkout
{
    //STEP 1 - WELCOME
    public static $guestEmailInput = '.newCustomer input[type="email"]';
    public static $newUserContinueButton = '.newCustomer .NewCustWrap';

    //STEP 2 - DELIVERY
    public static $firstNameInput = 'input[placeholder="Enter First Name"]';
    public static $lastNameInput = 'input[placeholder="Enter Last Name"]';

    public static $enterAddressManuallyLink = 'a.manuallyAddAddress';
    public static $addressFirstLine = 'input[data-placeholder="Start typing address or postcode"]';
    public static $addressTown = 'input[placeholder="Enter Town or City"]';
    public static $addressZip = 'input[placeholder="Enter Postcode/Zip"]';
    public static $addressMobile = 'input[placeholder="Enter Mobile Number"]';

    public static $toDeliveryButton = 'div.ProgressButContainTop .ContinueOn';

    public static $standartDeliveryRadioButton = 'li.DeliveryOptionsItem_EUR';
    public static $toPaymentButton = '.CheckoutLeft .AddressContainBut .ContinueOn';

    //STEP 3 - PAYMENT
    public static $payWithCardButton = '.CardsIcons';

    public static $cardIFrame = 'iframe[name="CardCaptureFrame"]';
    public static $cardNumber = '#card_number';
    public static $cardExpiryMonth= '#exp_month';
    public static $cardExpiryYear = '#exp_year_2digit';
    public static $cardCVV = '#cv2_number';

    public static $toConfirmButton = '#continue';

    //STEO 4 - CONFIRM
    public static $toSummaryButton = '.ContinueButtonWrapperTop';

}