<?php

namespace Page;

class ShoesPage
{
    public static $nikesForPurchaseURL = '/nike-class-cortez-nyl-l93-274062#colcode=27406202';
    public static $popUpCloseButton = '#advertPopup button';

    public static $productImage = '#pnlMainProductImage';
    public static $shoesSizeSelector = '.sizeButtonli';
    public static $addToBagButton = '.addToBag';
    public static $bagButton = '#bagQuantity';

}