<?php

class TesterData extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    //TEST PERSON
    public static $person = [
        'firstName' => 'Vārds',
        'surname' => 'Uzvārds',
        'email' => 'test@email.com'
    ];

    //TEST ADDRESS
    public static $address = [
        'firstLine' => 'Test addres first line',
        'city' => 'Test City',
        'zip' => '1111',
        'phone' => '22222222'
    ];

    //TEST DEFAULT VISA
    public static $visa = [
        'number' => '4111111111111111',
        'expMonth' => '10',
        'expYear' => '20',
        'cvv' => '737'
    ];
}